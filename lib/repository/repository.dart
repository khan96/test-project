import 'package:http/http.dart' as http;
import 'package:test_project/models/user_info.dart';

class Repository {
  static const baseUrl = "https://jsonplaceholder.typicode.com/users";

  Future<List<UserInfo>?> getUserName() async {
    var client = http.Client();

    var uri = Uri.parse(baseUrl);
    var response = await client.get(uri);
    if (response.statusCode == 200) {
      var json = response.body;
      return userInfoFromJson(json);
    }
    return null;
  }
}
