import 'package:flutter/material.dart';
import 'package:test_project/models/user_info.dart';
import 'package:test_project/repository/repository.dart';
import 'package:test_project/widgets/user.dart';

class UsersList extends StatelessWidget {
  var repo = Repository();

  final avatars = [
    'assets/images/avataaars(1).png',
    'assets/images/avataaars(2).png',
    'assets/images/avataaars(3).png',
    'assets/images/avataaars(4).png',
    'assets/images/avataaars(5).png',
    'assets/images/avataaars(6).png',
    'assets/images/avataaars(7).png',
    'assets/images/avataaars(8).png',
    'assets/images/avataaars(9).png',
    'assets/images/avataaars(10).png',
  ];

  UsersList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 150,
      child: FutureBuilder(
          future: repo.getUserName(),
          builder:
              (BuildContext context, AsyncSnapshot<List<UserInfo>?> snapshot) {
            if (snapshot.hasData) {
              List<UserInfo> user = snapshot.data!;
              return ListView.builder(
                itemCount: avatars.length,
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: User(
                    image: avatars[index],
                    name: user[index].name,
                  ),
                ),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
              );
            }
            return const Center(child: CircularProgressIndicator());
          }),
    );
  }
}
